import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    lastAddedId: 4,
    peoples: [
      { id: 1, name: 'John', gender: 'Male' },
      { id: 2, name: 'Sarah', gender: 'Female' },
      { id: 3, name: 'Tisha', gender: 'Female' },
      { id: 4, name: 'Grisha', gender: 'Male' },
    ],
    randomPeoples: [
      { name: 'Max', gender: 'Male' },
      { name: 'Lena', gender: 'Female' },
      { name: 'Veronika', gender: 'Female' },
      { name: 'Alex', gender: 'Male' },
      { name: 'Kostya', gender: 'Male' },
      { name: 'Anna', gender: 'Female' },
      { name: 'Kate', gender: 'Female' },
      { name: 'Boris', gender: 'Male' },
      { name: 'Masha', gender: 'Female' },
      { name: 'Dima', gender: 'Male' },
    ],
  },
  mutations: {
    addHuman(state, payload) {
      state.lastAddedId += 1;
      state.peoples.push({ id: state.lastAddedId, ...payload });
    },
    removeHuman(state, payload) {
      if (payload) {
        const index = state.peoples.findIndex(item => item.id === payload);
        state.peoples.splice(index, 1);
      } else {
        state.peoples.pop();
      }
    },
  },
  actions: {
    addHuman({ commit }) {
      const randomIndex = Math.floor(Math.random() * 10);
      const payload = this.state.randomPeoples[randomIndex];
      commit('addHuman', payload);
    },
    removeHuman({ commit }, payload) {
      commit('removeHuman', payload);
    },
  },
});
